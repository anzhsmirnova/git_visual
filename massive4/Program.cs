﻿using System;
/*Задан массив действительных чисел из N элементов. 
 * Определить количество элементов, значения которых находятся в диапазоне от -500 до +500. 
 * Решите задачу двумя способами: 
 * заполнение массива случайными числами или заполнение с клавиатуры.
*/
namespace massive1
{
    internal class Program
    {
        static void Main()
        {
            int result = 0;

            int len = Convert.ToInt32(Console.ReadLine());

            var array = new int[len];

            Random r = new Random();

            for (int i = 0; i < array.Length; i++)

            {
                array[i] = r.Next(-1000, 1000);
            }

            Console.WriteLine(String.Join(" - ", array));

            
            Console.WriteLine("Find [-501,500] - {0}", Array.FindAll<int>(array, item => item < 501 || item > -501));

            

            Console.ReadKey();
        }
    }
}

