﻿using System;
using System.ComponentModel;
/*Задана квадратная матрица целых чисел. Подсчитайте количество отрицательных и положительных элементов, 
количество нулевых элементов, сумму элементов по главной диагонали. Массив заполнять случайным образом.
*/
namespace massive3
{
    internal class Program
    {
        static void Main()
        {
            // Компилятор определяет размер массива на основании выражения инициализации.


            int[,] array = {

                                { -1, 2, -3 },

                                { 4, -5, 6 },

                                { 0, 8, 0 }

                            };

            Console.WriteLine("Find - {0}", Array.Find(array, item => item < 0));
            PrintIntArray(Array.FindAll(array, item => item < 0));
        }

        
        


            /*for (int i = 0; i < 3; i++)

            {

                for (int j = 0; j < 3; j++)

                {

                    Console.Write("{0} ", array[i, j]);

                }

                Console.Write("\n");

            }
            */

            // Delay

            Console.ReadKey();

        }

    }

}