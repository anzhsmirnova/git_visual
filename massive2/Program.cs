﻿using System;
/*Найти среднее арифметическое n элементов одномерного массива с размерностью N. 
Количество элементов ввести с клавиатуры, массив заполнить случайными числами.
*/

namespace massive2
{
    internal class Program
    {
        static void Main()
        {
            int result = 0; 

            int len = Convert.ToInt32(Console.ReadLine());

            var arrey = new int[len];

            Random r = new Random();

            for (int i = 0; i < arrey.Length; i++)

            {
                arrey[i] = r.Next(0, 1000);
            }

            Console.WriteLine(String.Join(" - ", arrey));

            foreach (var num in arrey)
            {
                result += num;
            }

            Console.WriteLine($"{result / len}");

            Console.ReadKey();
        }
    }
}
