﻿using System;
/*Найти сумму элементов массива из n вещественных чисел. 
Количество элементов ввести с клавиатуры, массив заполнить случайными числами.
*/
namespace massive1
{
    internal class Program
    {
        static void Main()
        {
            int result = 0;
            
            int len = Convert.ToInt32(Console.ReadLine());

            var array = new int[len];

            Random r = new Random();
            
            for (int i = 0; i < array.Length; i++)

            {
                array[i] = r. Next(0, 1000);
            }

            Console.WriteLine(String.Join (" - ", array));

            foreach (var num in array)
            {
                result += num;
            }
            Console.WriteLine(result);
            
            Console.ReadKey();
        }
    }
}
