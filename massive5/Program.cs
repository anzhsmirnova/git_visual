﻿using System;
using System.Collections.Generic;
/*Задан массив действительных чисел из N элементов. 
* Определить количество элементов, значения которых находятся в диапазоне от -500 до +500. 
* Решите задачу двумя способами: 
* заполнение массива случайными числами или заполнение с клавиатуры.
*/
namespace massive1
{
    internal class Program
    {
        static void Main()
        {
            int len = Convert.ToInt32(Console.ReadLine());

            var array = new int[len];

            for (int i = 0; i < array.Length; i++)

            {
                array[i] = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine(String.Join(" - ", array));
            }
            
            Console.WriteLine("Find [-502,500] - {0}", Array.Find(array, item => item < 501 || item > -501));

            Console.ReadKey();
        }
    }
}
