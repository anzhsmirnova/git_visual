﻿// обынчый
using System;
using System.Linq;

var array1 = new int[10];
var array2 = new string[3] { "1", "два", "три" };
var array3 = new string[] { "1", "два" };
int[] array4 = { 1, 2, 3, 4, 5, 6 };


// многомерный
int[,] a2 = new int[3, 3];

a2[0, 0] = 1;
a2[0, 1] = 2;
a2[0, 2] = 3;

a2[1, 0] = 4;
a2[1, 1] = 5;
a2[1, 2] = 6;

a2[2, 0] = 7;
a2[2, 1] = 8;
a2[2, 2] = 9;

//Print2DMatrixArray(a2);

// зубчатый
int[][] zArray = new int[2][];

zArray[0] = new int[] { 1, 2, 3, 4, 5 };
zArray[1] = new int[] { 1, 2 };

//PrintZArray(zArray);

//PrintArray(array1);
//PrintArray(array2);
//PrintArray(array3);



// рандомизация массива
int[] ranArray = new int[10];

var newRandomizeArray = SetRandomizeArrayValue(ranArray);

//PrintIntArray(newRandomizeArray);


// Методы массивов
Console.WriteLine("Length - {0}", array3.Length);

Console.WriteLine("Rank 2мерный - {0}", a2.Rank);
Console.WriteLine("Rank - {0}", array3.Rank);

Console.WriteLine("Contains - {0}", array3.Contains("двsа"));

Console.WriteLine("Find - {0}", Array.Find(ranArray, item => item < 10));

PrintIntArray(Array.FindAll(ranArray, item => item < 1));
PrintIntArray(array1);

Array.Resize(ref array1, 2);

PrintIntArray(array1);

Console.WriteLine("IndexOf - {0}", Array.IndexOf(array3, "два"));

PrintArray(array3);
PrintArray(array3.Reverse().ToArray());


PrintIntArray(ranArray);
Array.Sort(ranArray);
PrintIntArray(ranArray);

Console.WriteLine(Array.BinarySearch(ranArray, 0, 10, 1));





//PrintIntArray(ranArray);
//Array.Clear(ranArray);
//PrintIntArray(ranArray);

//Array.Copy(ranArray, array1, 5);

//PrintIntArray(array1);


void PrintZArray(int[][] arrays)
{
    Console.WriteLine("Длина массива - {0}", arrays.Length);


    foreach (var array in arrays)
    {
        foreach (var item in array)
        {
            Console.WriteLine("из Foreach - {0}", item);
        }
    }
}

int[] SetRandomizeArrayValue(int[] array)
{
    Random r = new Random();

    for (int i = 0; i < array.Length; i++)
    {
        array[i] = r.Next(-10, 10);
    }

    return array;
}

void PrintArray(string[] array)
{
    Console.WriteLine("Длина массива - {0}", array.Length);


    foreach (var item in array)
    {
        Console.WriteLine("из Foreach - {0}", item);
    }
}

void PrintIntArray(int[] array)
{
    Console.WriteLine("Длина массива - {0}", array.Length);


    foreach (var item in array)
    {
        Console.Write("{0} - ", item);
    }

    Console.WriteLine("");
}

void Print2DArray(int[,] array)
{
    Console.WriteLine("Длина массива - {0}", array.Length);


    foreach (var item in array)
    {
        Console.WriteLine("из Foreach - {0}", item);
    }
}

void Print2DMatrixArray(int[,] array)
{
    Console.WriteLine("Длина массива - {0}", array.Length);


    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            Console.Write(array[i, j]);
        }

        Console.WriteLine("");
    }
}

// ctrl + K + C - Закоментировать
// ctrl + K + U - Раскоментировать
//int index = Convert.ToInt32(Console.ReadLine());
//string word = Console.ReadLine();

//array[index] = word;

//for (int i = 0; i < array.Length; i++)
//{
//    Console.WriteLine("{0} - {1}", i, array[i]);
//}