﻿using System;
/*Задана квадратная матрица целых чисел. Подсчитайте количество отрицательных и положительных элементов, 
количество нулевых элементов, сумму элементов по главной диагонали. Массив заполнять случайным образом
*/
namespace task5
{
    internal class Program
    {
        static void Main()
        {
            const int n = 5, m = 5;

            int[,] Mas1 = new int[m, n];
           
            Random rand = new Random();

            for (int j = 0; j < n; j++)
            {
                for (int i = 0; i < n; i++)
                {
                    Mas1[i, j] = rand.Next(-10, 10);
                    Console.Write(Mas1[i, j]);
                }

                Console.WriteLine();
            }
                    foreach (int item in Mas1)
                    {
                        Console.WriteLine(item);
                    }

            Console.WriteLine("Find < 0 - {0}", Array.Find(Mas1, item => item < 0));

            Console.WriteLine("Find > 0 - {0}", Array.Find(Mas1, item => item > 0));

            Console.ReadKey();
        }
    } 
}
